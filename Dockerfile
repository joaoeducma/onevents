FROM python:3.6-alpine

WORKDIR /usr/src

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN ["chmod", "+x", "/usr/src/start.sh"]
CMD ["./start.sh"]

