# on_events

A very simple web-system for managing events registration and subscriptions.

---
## "Manual" Installation

A the repository root run the following commands:

```
$ python -m venv ../preferred/path/to/venv
$ . ../preferred/path/to/venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ coverage run --omit='*migrations*,*apps.py,*wsgi.py,*manage.py' --source='.' manage.py test && coverage report
$ python manage.py populate   # only if you want to populate the database with dummy records.
$ python manage.py runserver
```
## Docker Installation

The actual docker commands are inside a Makefile.

The `make start` command mounts your host-machine repo folder in the docker instance, so that you can edit your code from the outside and make changes in the instance.

```
# make sure that docker service is running on your machine
$ cd ~/to/this/repo/root
$ make build-image
$ make start
# Once inside the docker instance you can...
$ python manage.pt test/populate/runsever/shell etc.
```

#### Disclaimer about possible SQLite database problem!

I've stumbled upon [Django recent problems with SQLite 3.26+](https://code.djangoproject.com/ticket/29182).
If you're running on the most recent version of SQLite, it is quite possible that it will not work with Django 2.X (which I have used for this project). [Django 2.1.5](https://docs.djangoproject.com/en/2.1/releases/2.1.5/) (to be release first week of Jan, 2019 ) should fix this issue.

I've applied a Monkey Patch that introduces the fixe presented [here](https://github.com/django/django/commit/c8ffdbe514b55ff5c9a2b8cb8bbdf2d3978c188f), by the Django team. See `on_events/monkey.py`.

---

## Core Business Requirements

- The system user (which must be properly logged-in) must be able to register events.
- The system must present a index of the upcoming events.
- The system must present a detailed view of a single event.
- Visitors on the platform (unregistered users) must be able to subscribe to a event, inputing the name, email and optionally leaving comment.


## Additional/Subtler Business Requirements (implies Test Cases)

- There can't be two events with the same slug.
- The Same email can't be subscribed twice for the same event.
- Past events can't show up on the default listing.
- Non-published events can't show up on the default listing.
- Non-published events can't be accessible to non-authenticated users (404), but can be by logged users.
- Subscribers comments can't be longer than 256 characters.
- i18n must be routed through a friendly url.
- Events schedule dates can't be previous to the creation date.
- In the detail view we should list only the last 15 subscribers, but give a total count.
- Ability download event ics file.
- Send a confirmation e-mail upon subscription with the event ics attached to it.
- Events must have a location string.


## Further improving on the solution

- E-mail notifications (when there are changes made to the event, send a email to the subscribers).
- E-mail reminders.
- Location, 3 fields: shorthand (required), address and lat/lon (optional).
- Option to Limit attendees for a given event.
- Custom management command for sending pending emails.
- Schema.org Event markup.
