"""The dummy data factory."""
from django.utils.text import slugify
from random import choice, randint
from datetime import datetime, timedelta

ADJECTIVES = ["Brutal", "Mortal", "Lovely", "Terrific", "Very Serious", "Blissful", "Grueling", "Horrific", "Cursed",
              "Romantic", "Festive", "Epic", "Daunting", "Unforgivable", "", "Peaceful", "Strenuous", "Hilarious",
              "Insightful", "Chaotic", "Exotic", "Wild", "Contemplative"]

EVENTS = ["Poker Night", "Chef's Day", "Django Training", "ReactJS Workshop", "Yoga Session", "Cine Club Meeting",
          "Reading Club Meeting", "Pool Party", "IT Barbecue", "Ice Cream Festival", "Product Presentation",
          "Photo Shooting", "Ping Pong Tournament", "Street Running", "Arm Wrestling", "Break-Dance Championship",
          "Scrum Training", "Chorus Practicing", "Paint-Ball Mayhem", "Parkour Session", "Sushi Bar", "Pizza Day",
          "Salsa Showdown", "Meditation Session", "Massage Session", "Spa Morning", "Mario Kart Tournament"]

LOCATIONS = ["auditorium", "W Hotel Auditorium", "nearby park", "office", "kitchen", "attic", "basement", "Camp Nou",
             "Park Güell"]

NAMES = ["Anderson", "Andre", "Antony", "Augusto", "Amy", "Andrea", "Ana", "Aline", "Ariel", "Lucas", "Luke",
         "Bob", "Bruno", "Bento", "Bruna", "Brenda", "Barbara", "Briana", "Beatriz", "Paulo", "Paula", "Leia",
         "Julio", "Julia", "Carlos", "Carla", "Fernando", "Fernanda", "Marcia", "Márcia", "Thiago", "John", "Paul",
         "George", "Priscila", "Fabio", "Flavia", "Flavio", "Alberto", "Ricardo", "Frederico", "Igor", "Mario",
         "Luigi", "Caio", "Cristina", "Daniel", "Gabriel", "Gabriela"]

LASTNAMES = ["Allyrion", "Arryn", "Ashford", "Baelish", "Baratheon", "Beesbury", "Belgrave", "Blackfyre", "Blackmont",
             "Blacktyde", "Blackwood", "Blount", "Bolton", "Botley", "Bracken", "Branfield", "Bulwer", "Cargyll",
             "Caron", "Cassel", "Casterly", "Caulfield", "Cerwyn", "Clegane", "Connington", "Corbray", "Crakehall",
             "Cuy", "Dalt", "Darry", "Dayne", "Dondarrion", "Dormund", "Durrandon", "Dustin", "Egen", "Erenford",
             "Estermont", "Florent", "Forrester", "Fossoway", "Frey", "Glover", "Greenfield", "Greyjoy", "Harlaw",
             "Harroway", "Hightower", "Hoare", "Hollard", "Holt", "Hornwood", "Hunter", "Jordayne", "Justman",
             "Karstark", "Kenning", "Lannister", "Lefford", "Lorch", "Lothston", "Lynderly", "Mallister", "Manwoody",
             "Marbrand", "Marsh", "Martell", "Mollen", "Moore", "Mooton", "Mormont", "Morrigen", "Mudd"]

DOMAINS = ["outlook.com", "hotmail.com", "gmail.com", "yahoo.com", "icloud.com", "facebook.com",
           "suspiciousdomain.com", "opportunitynetwork.com"]

SEPS = ["", ".", "_", "__"]

COMMENTS = ["it is me!", "Hey! Ho!", "Hasta la vista!"]


def random_event(past=False):
    title = "%s %s" % (choice(ADJECTIVES), choice(EVENTS))
    loc = choice(LOCATIONS)
    desc = "arandomdesc"
    days = randint(3, 90)
    if past:
        days *= -1

    dt = datetime.now() + timedelta(days=days)

    return title.strip(), loc, desc, dt


def random_attendee():
    fn, ln, sp, dm = choice(NAMES), choice(LASTNAMES), choice(SEPS), choice(DOMAINS)
    name = "%s %s" % (fn, ln)
    email = "%s%s%s@%s" % (slugify(fn), sp, slugify(ln), dm)
    comment = choice(COMMENTS)

    return name, email, comment
