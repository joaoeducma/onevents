"""Populate the database with dummy data."""
from random import randint
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings
from events.models import Event, Subscription
from django.core.exceptions import ValidationError
from . import dummy


settings.ALLOW_PAST_EVENTS_CREATION = True


class Command(BaseCommand):

    def wipe(self):
        print("Wiping Subscriptions")
        Subscription.objects.all().delete()

        print("Wiping Events")
        Event.objects.all().delete()

        try:
            print("Wiping dummy_admin")
            User.objects.get(username="dummy_admin").delete()
        except User.DoesNotExist:
            pass

    def handle(self, *args, **options):
        self.wipe()

        print("Populating the database with dummy data")
        adm = User.objects.create(username="dummy_admin", email="dummy.admin@onevents.com")

        # Create 10 published, upcoming  (5-99 subscribers)
        # Create 10 unpublished, upcoming  (no-subscribers)
        # Create 10 purblished, previous  (5-99 subscribers),
        for i in range(30):

            past = i > 20
            published = 10 > i or past

            title, loc, desc, dt = dummy.random_event(past)
            print(title)
            try:
                ev = Event.objects.create(
                    title=title,
                    location=loc,
                    description=desc,
                    schedule_date=dt,
                    published=published,
                    author=adm
                )
            except ValidationError:
                continue

            if not published:
                continue

            subs = []
            for i in range(randint(5, 99)):
                name, email, comment = dummy.random_attendee()
                subs.append(Subscription(name=name, email=email, comment=comment, event=ev))

            Subscription.objects.bulk_create(subs)
