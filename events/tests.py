"""Unit tests for the application."""
from django.test import TestCase
from .models import Event, Subscription
from django.utils.text import slugify
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import Client
from django.conf import settings
from django.urls import reverse
from events.management.commands.populate import Command as Pop
# from django.utils import translation
# translation.activate('en')


class EventCreationTestCase(TestCase):

    def setUp(self):
        self.admin_user = User.objects.create(
            username="test_admin",
            email="test_admin@onevents.com"
        )

        title = "a sample event"
        slug = slugify(title)
        self.schedule_date = datetime.now() + timedelta(days=4)

        Event.objects.create(
            title=title,
            slug=slug,
            location="the usual",
            description="description here",
            schedule_date=self.schedule_date,
            author=self.admin_user
        )

    def test_unique_event_slug(self):
        p_event = Event.objects.latest("pk")

        with self.assertRaises(ValidationError):
            Event.objects.create(
                title="a brand new event",
                slug=p_event.slug,
                location="the usual",
                description="description here",
                schedule_date=self.schedule_date,
                author=self.admin_user
            )

    def test_no_blank_event_title_or_slug(self):

        with self.assertRaises(ValidationError):
            Event.objects.create(
                title="",
                slug="i_have_a_slug",
                location="the usual",
                description="description here",
                schedule_date=self.schedule_date,
                author=self.admin_user
            )

        # The slug is autofilled by save() method
        Event.objects.create(
            title="i have a title",
            slug="",
            location="the usual",
            description="description here",
            schedule_date=self.schedule_date,
            author=self.admin_user
        )

    def test_no_bad_slug(self):

        with self.assertRaises(ValidationError):
            Event.objects.create(
                title="i have a slug",
                slug="i have a slug",
                location="the usual",
                description="description here",
                schedule_date=self.schedule_date,
                author=self.admin_user
            )

    def test_new_events_only_on_future_dates(self):

        with self.assertRaises(ValidationError):
            Event.objects.create(
                title="i_have_a_slug",
                slug="i_have_a_slug",
                location="the usual",
                description="description here",
                schedule_date=datetime.now() - timedelta(days=1),
                author=self.admin_user
            )


class SubscriptionCreationTestCase(TestCase):

    def setUp(self):
        self.admin_user = User.objects.create(
            username="test_admin",
            email="test_admin@onevents.com"
        )

        title = "a sample event"
        slug = slugify(title)
        self.schedule_date = datetime.now() + timedelta(days=4)

        self.p_event = Event.objects.create(
            title=title,
            slug=slug,
            location="the usual",
            description="description here",
            schedule_date=self.schedule_date,
            author=self.admin_user
        )

    def test_bad_email_format(self):
        with self.assertRaises(ValidationError):
            Subscription.objects.create(
                name="joão maia",
                email="bad_email_onevents.com",
                comment="a comment",
                event=self.p_event
            )

    def test_no_blank_name_or_email(self):
        with self.assertRaises(ValidationError):
            Subscription.objects.create(
                name="",
                email="testuser@onevents.com",
                comment="a comment",
                event=self.p_event
            )

        with self.assertRaises(ValidationError):
            Subscription.objects.create(
                name="a name",
                email="",
                comment="a comment",
                event=self.p_event
            )

    def test_unique_email_per_event(self):

        Subscription.objects.create(
            name="test user",
            email="testuser@onevents.com",
            comment="a comment",
            event=self.p_event
        )

        with self.assertRaises(ValidationError):
            Subscription.objects.create(
                name="another test user",
                email="testuser@onevents.com",
                comment="a comment",
                event=self.p_event
            )

    def test_comment_max_length(self):
        with self.assertRaises(ValidationError):
            lengthy_comment = """
                this is a very lengthy comment indeed, Captain.
                this is a very lengthy comment indeed, Captain.
                this is a very lengthy comment indeed, Captain.
                this is a very lengthy comment indeed, Captain.
                this is a very lengthy comment indeed, Captain.
            """

            Subscription.objects.create(
                name="test user",
                email="testuser@onevents.com",
                comment=lengthy_comment,
                event=self.p_event
            )


def populate(create_admin=True, near=False):
    admin_user = User.objects.create(
        username="test_admin",
        email="test_admin@onevents.com"
    )

    title = "a sample event%s" % (" near date" if near else "")
    slug = slugify(title)
    if near:
        schedule_date = datetime.now() + timedelta(hours=18)
    else:
        schedule_date = datetime.now() + timedelta(days=4)

    # EVENT A
    Event.objects.create(
        title=title,
        slug=slug,
        location="the usual %s",
        description="description here",
        schedule_date=schedule_date,
        author=admin_user,
        published=True
    )

    title = "unpublished event%s" % (" near date" if near else "")
    slug = slugify(title)

    # EVENT B
    Event.objects.create(
        title=title,
        slug=slug,
        location="the usual",
        description="description here",
        schedule_date=schedule_date,
        author=admin_user,
        published=False
    )

    # Getting around the validation for not creating new events with past dates!
    settings.ALLOW_PAST_EVENTS_CREATION = True

    # EVENT C
    title = "a-previous-event%s" % ("-near-date" if near else ""),
    Event.objects.create(
        title=title,
        slug=slugify(title),
        location="the usual",
        description="description here",
        schedule_date=datetime.now() - timedelta(days=5),
        created_at=datetime.now() - timedelta(days=7),
        updated_at=datetime.now() - timedelta(days=7),
        author=admin_user,
        published=True
    )
    settings.ALLOW_PAST_EVENTS_CREATION = False

    return admin_user


class IndexViewTestCase(TestCase):

    route = reverse('index')

    def setUp(self):
        populate(create_admin=False, near=True)

        self.cli = Client()
        self.resp = self.cli.get(self.route)

    def test_200(self):
        self.assertTrue(self.resp.status_code == 200)

    def test_published_events_only(self):
        self.assertTrue(len(self.resp.context_data['events']) == 1)
        for ev in self.resp.context_data['events']:
            self.assertEqual(ev['published'], True)

    def test_future_events_only(self):

        self.assertTrue(len(self.resp.context_data['events']) == 1)
        for ev in self.resp.context_data['events']:
            self.assertTrue(ev['schedule_date'] > datetime.now())


class PrevioslyViewTestCase(IndexViewTestCase):
    route = reverse('previously')

    def test_future_events_only(self):
        pass

    def test_past_events_only(self):

        self.assertTrue(len(self.resp.context_data['events']) == 1)
        for ev in self.resp.context_data['events']:
            self.assertTrue(ev['schedule_date'] < datetime.now())


class DetailViewTestCase(TestCase):

    def setUp(self):
        self.admin = populate()
        self.cli = Client()

    def test_200(self):
        for ev in Event.objects.filter(published=True):
            route = reverse('event', args=[ev.slug])
            resp = self.cli.get(route)
            self.assertEqual(resp.status_code, 200)

    def test_404_for_unpublished(self):
        for ev in Event.objects.filter(published=False):
            route = reverse('event', args=[ev.slug])
            resp = self.cli.get(route)
            self.assertEqual(resp.status_code, 404)

    def test_200_for_unpublished_when_logged_in(self):
        self.cli.force_login(self.admin)
        for ev in Event.objects.filter(published=False):
            route = reverse('event', args=[ev.slug])
            resp = self.cli.get(route)
            self.assertEqual(resp.status_code, 200)

    def test_limit_retrieved_subscribers(self):
        event = Event.objects.latest("pk")
        for idx in range(1, 20):
            Subscription.objects.create(
                name="another test user %d" % idx,
                email="testuser%d@onevents.com" % idx,
                comment="a comment",
                event=event
            )

        route = reverse('event', args=[event.slug])
        resp = self.cli.get(route)
        self.assertEqual(
            len(resp.context_data['subscriptions']),
            settings.DETAIL_VIEW_RETRIEVED_SUBSCRIPTIONS
        )
        self.assertEqual(resp.context_data['subscriptions_count'], 19)


class DetailViewSubscriptionPostTestCase(TestCase):

    def setUp(self):
        self.admin = populate()
        self.cli = Client()

    def test_200(self):
        event = Event.objects.latest("pk")
        route = reverse('event', args=[event.slug])
        for idx in range(1, 20):
            resp = self.cli.post(route, {
                's_name': "another test user %d" % idx,
                's_email': "testuser%d@onevents.com" % idx,
                'comment': "a comment"
            })
            self.assertEqual(resp.status_code, 200)
            self.assertFalse('messages' in resp.context_data)

    def test_404(self):
        route = reverse('event', args=['non-existent-event'])
        idx = 21
        resp = self.cli.post(route, {
            's_name': "another test user %d" % idx,
            's_email': "testuser%d@onevents.com" % idx,
            'comment': "a comment"
        })
        self.assertEqual(resp.status_code, 404)

    def test_404_for_unpublished(self):

        for event in Event.objects.filter(published=False):
            route = reverse('event', args=[event.slug])
            idx = 21
            resp = self.cli.post(route, {
                's_name': "another test user %d" % idx,
                's_email': "testuser%d@onevents.com" % idx,
                'comment': "a comment"
            })
            self.assertEqual(resp.status_code, 404)

    def test_200_for_unpublished_when_logged_in(self):
        self.cli.force_login(self.admin)

        for event in Event.objects.filter(published=False):
            route = reverse('event', args=[event.slug])
            idx = 21

            resp = self.cli.post(route, {
                's_name': "another test user %d" % idx,
                's_email': "testuser%d@onevents.com" % idx,
                'comment': "a comment"
            })
            self.assertEqual(resp.status_code, 200)
            self.assertFalse('messages' in resp.context_data)

    def test_error_bad_request(self):

        event = Event.objects.latest("pk")
        route = reverse('event', args=[event.slug])
        idx = 22
        resp = self.cli.post(route, {
            's_name': "",
            's_email': "testuser%d@onevents.com" % idx,
            'comment': "a comment"
        })
        self.assertTrue(True in ["Validation error" in m.message for m in list(resp.context['messages'])])


class DetailIcsViewTestCase(TestCase):

    def setUp(self):
        self.admin = populate()
        self.cli = Client()

    def test_200(self):
        event = Event.objects.latest("pk")
        route = reverse('event_ics', args=[event.slug])
        resp = self.cli.get(route)
        ctt = resp.content.decode("utf-8")

        self.assertTrue(event.title in ctt)
        self.assertFalse("09-SD9+NDSN129321M,CXCDF0D9F0" in ctt)

    def test_404(self):
        route = reverse('event_ics', args=['non-existent-event'])
        resp = self.cli.get(route)

        self.assertEqual(resp.status_code, 404)


class PopulateCommandTestCase(TestCase):

    def test_200(self):
        Pop().handle()
        for ev in Event.objects.all():
            print(ev)
            for sub in ev.subscription_set.all():
                print(sub)
