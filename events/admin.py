from django.contrib import admin
from .models import Event, Subscription


class EventAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'slug', 'author', 'schedule_date', 'published', 'created_at')
    ordering = ('-schedule_date',)


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')


admin.site.register(Event, EventAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
