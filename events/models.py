"""The Data models for the project."""
from django.db import models
from datetime import datetime
from django.utils.text import slugify
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError
from django.conf import settings


class Event(models.Model):

    title = models.CharField(max_length=128, blank=False)
    slug = models.SlugField(max_length=128, unique=True)

    description = models.CharField(max_length=512, blank=False)
    location = models.CharField(max_length=128, blank=False)
    schedule_date = models.DateTimeField(null=False)
    published = models.BooleanField(default=False)
    author = models.ForeignKey(User, on_delete=models.PROTECT)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.slug

    @property
    def is_past(self):
        return self.time_until.total_seconds() < 0

    @property
    def time_until(self):
        """Calculate the time remaining-to/since the event."""
        return self.schedule_date - datetime.now()

    @property
    def time_until_display(self):
        """Humanize the time_until information."""
        tu = self.time_until
        if tu.total_seconds() < 0:
            return _("past")

        if tu.days > 0:
            return "%d %s" % (tu.days, _("days"))

        hh, mm = str(tu).split(":")[:2]

        return "%s %s %s %s %s" % (hh, _("hours"), _("and"), mm, _("minutes"))

    @property
    def flyweight(self):
        """I use this pattern to avoid sending live-model-objects directly into the template."""
        return {
            "title": self.title,
            "slug": self.slug,
            "description": self.description,
            "location": self.location,
            "schedule_date": self.schedule_date,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
            "published": self.published,
            "author": self.author.username,
            "prop__time_until_display": self.time_until_display,
            "prop__is_past": self.is_past,
        }

    def clean(self):
        """Validation for no past date for new events. If someone REALLY need to do it, you can still do it via SQL."""
        if not settings.ALLOW_PAST_EVENTS_CREATION and self.schedule_date < datetime.now():
            raise ValidationError("New events should be created for future dates only!")

    def save(self, *args, **kwargs):
        """Forcing the full_clean, and auto-filling the slug, if not presented."""
        if not self.slug:
            self.slug = slugify(self.title)

        self.full_clean()
        super(Event, self).save(*args, **kwargs)


class Subscription(models.Model):

    name = models.CharField(max_length=64, blank=False)
    email = models.EmailField(blank=False)
    comment = models.CharField(max_length=256, blank=True)
    event = models.ForeignKey(Event, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def firstname(self):
        return self.name.split(" ")[0]

    class Meta:
        unique_together = (("email", "event"),)

    def __str__(self):
        return "%s (%s)" % (self.email, self.firstname)

    @property
    def flyweight(self):
        return {
            "name": self.name,
            "email": self.email,
            "comment": self.comment,
            "event__id": self.event.pk,
            "created_at": self.created_at,
            "updated_at": self.updated_at,
        }

    def save(self, *args, **kwargs):
        """Forcing the full_clean."""
        self.full_clean()
        super(Subscription, self).save(*args, **kwargs)
