"""Implements the views (business logic for url endpoints) for the core of the system."""
from datetime import timedelta, datetime
from django.views.generic import View, TemplateView
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.http import Http404, HttpResponse
from django.conf import settings
from django.utils.translation import gettext as _
from django.core.mail import EmailMessage
from .models import Event, Subscription
from .ics_template import TPL


# TODO: MOVE TO PROJECT SETTINGS?
TITLE_PREFIX = "ON-EVENTS"


def render_ics(event):
    """Given a event object, retrieves the ics file content for it.

    Arguments:
    event -- Event model object

    Returns:
    filename -- The name to be used for the attachment
    content  -- The actual text content for the .ics file
    """
    filename = "%s.ics" % event.slug.replace("-", "_")

    # For now all events have a 1 hour duration
    end_dt = event.schedule_date + timedelta(hours=1)

    return filename, TPL % ({
        "event_title": event.title,
        "event_location": event.location,
        "event_description": event.description,
        "event_date_begin": event.schedule_date.strftime("%Y%m%dT%H%M00"),
        "event_date_end": end_dt.strftime("%Y%m%dT%H%M00"),
    })


def send_subscription_mail(request, event, subscription):
    """Send a email confirming the subscription to the subscribed email.

    Arguments:
    request -- django request object
    event -- Event model object
    subscription -- Subscription model object
    """
    subj = '%s %s' % (_("Successfully subscribed to"), event.title)
    txt_content = '%s, %s...' % (_("Greetings"), subscription.firstname)

    filename, ics = render_ics(event)

    emsg = EmailMessage(
        subj,
        txt_content,
        from_email='do-not-reply@onevents.com',
        to=[subscription.email],
        attachments=[(filename, ics, "text/ics",)]
    )
    emsg.send()

    msg = "%s '%s' (or the RUNSERVER CONSOLE, if in DEV)" % (
        _("A confirmation e-mail was sent to"), subscription.email)

    messages.success(request, msg)


class BaseTemplateView(TemplateView):

    nav = "base"

    @property
    def _events(self):
        return []

    def get_context_data(self, **kwargs):
        return {
            "title": self.title,
            "description": self.description,
            "events": self._events,
            "nav": self.nav
        }


class Index(BaseTemplateView):
    """
    Handle the requests for the upcoming events index/list.

    Only published and future events should-be returned.
    """

    nav = "index"
    template_name = "events/index.html"

    @property
    def _events(self):
        """Isolate the fetching logic allowing subclasses to re-use more code."""
        events = Event.objects.filter(
            published=True,
            schedule_date__gte=datetime.now()
        ).order_by('schedule_date')

        return [x.flyweight for x in events]

    @property
    def title(self):
        return "%s - %s" % (TITLE_PREFIX, _("Upcoming Events"))

    @property
    def description(self):
        return "%s ON-EVENTS" % _("List of the upcoming events on")


class Previously(BaseTemplateView):
    """
    Handle the requests for the PREVIOUS events index/list.

    Only published and PAST events should-be returned.
    """

    nav = "previously"
    template_name = "events/previously.html"

    @property
    def title(self):
        return "%s - %s" % (TITLE_PREFIX, _("Previous Events"))

    @property
    def description(self):
        return "%s ON-EVENTS" % _("List of the previous events on")

    @property
    def _events(self):
        events = Event.objects.filter(published=True, schedule_date__lte=datetime.now())
        return [x.flyweight for x in events]


class Detail(BaseTemplateView):
    """
    Handle the requests for the detailed view of a given event.

    Notice that the settings variable DETAIL_VIEW_RETRIEVED_SUBSCRIPTIONS limits the amount of subscriptions previewed
    in the template.

    This class also implements the subscription POST request. Creating a new subscription in the database, a giving
    the appropriate feedbacks using the django Messages sub-system.
    """

    event = None
    template_name = "events/view.html"

    @property
    def title(self):
        return "%s @ %s - %s" % (self.event.title, self.event.location, TITLE_PREFIX),

    @property
    def description(self):
        return self.event.description

    def get_event(self, slug):
        # In case the self.event was already fetched (e.g: in the post handler)
        if not self.event:
            self.event = get_object_or_404(Event, slug=slug)

        if not self.event.published and not self.request.user.is_authenticated:
            raise Http404(_("Event not found"))

    def get_context_data(self, **kwargs):
        self.get_event(kwargs['slug'])
        ctx = super(Detail, self).get_context_data(*[], **kwargs)

        # Sub-optimal solution
        # SHOULD BE: a single-query which retrieves the records limited, and the total count.
        subs = self.event.subscription_set.all().order_by("-created_at")
        cnt = subs.count()
        subs = subs[0:settings.DETAIL_VIEW_RETRIEVED_SUBSCRIPTIONS]

        ctx.update({
            "event": self.event.flyweight,
            "subscriptions": [x.flyweight for x in subs],
            "subscriptions_count": cnt
        })

        return ctx

    @csrf_exempt
    def post(self, request, slug):
        self.get_event(slug)

        name = request.POST['s_name']
        email = request.POST['s_email']

        try:
            subs = Subscription.objects.create(name=name, email=email, comment=request.POST['comment'],
                                               event=self.event)
            msg = '%s! %s, %s!' % (_("Successfully subscribed"), name, _("see you there"))
            messages.success(request, msg)
            send_subscription_mail(request, self.event, subs)
        except ValidationError as ex:
            for key, values in ex.message_dict.items():
                if key.startswith("_"):
                    continue

                for val in values:
                    prefix = _("Validation error on field")
                    messages.error(request, '%s "%s": %s' % (prefix, key, val))

        return self.render_to_response(self.get_context_data(**{'slug': slug}))


class DetailIcs(View):
    """Very straigth forward view that delivers the .ics file as a attachment/download for a given event slug."""

    def get(self, request, slug):
        event = get_object_or_404(Event, slug=slug)
        filename, ics = render_ics(event)
        resp = HttpResponse(ics, "text/ics")
        resp['Content-Disposition'] = 'attachment; filename=%s;' % filename

        return resp
