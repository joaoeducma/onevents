"""
Workaround depencies bugs.

https://github.com/django/django/commit/c8ffdbe514b55ff5c9a2b8cb8bbdf2d3978c188f
"""
from django.db.backends.sqlite3 import schema


class DatabaseSchemaEditorMP(schema.DatabaseSchemaEditor):

    def __enter__(self):
        # Some SQLite schema alterations need foreign key constraints to be
        # disabled. Enforce it here for the duration of the transaction.
        self.connection.disable_constraint_checking()
        self.connection.cursor().execute('PRAGMA legacy_alter_table = ON')
        return super().__enter__()

    def __exit__(self, exc_type, exc_value, traceback):
        super().__exit__(exc_type, exc_value, traceback)
        self.connection.cursor().execute('PRAGMA legacy_alter_table = OFF')
        self.connection.enable_constraint_checking()


def apply():
    schema.DatabaseSchemaEditor = DatabaseSchemaEditorMP
