#!/usr/bin/env sh

set -e

python /usr/src/manage.py migrate
python /usr/src/manage.py test
python /usr/src/manage.py populate
python /usr/src/manage.py runserver 0.0.0.0:8000

