.PHONY: build-image start logs remove

build-image:
	docker build -t onevents .

start:
	docker run -p 8001:8000 \
	--name onevents-instance \
	--mount type=bind,source="${CURDIR}",target=/usr/src \
	-it --rm onevents sh

logs:
	docker logs -f onevents-instance

remove:
	docker stop onevents-instance; docker rm onevents-instance
